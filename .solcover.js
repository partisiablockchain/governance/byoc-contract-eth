module.exports = {
  providerOptions: {
    // Running solidity-coverage extracts gas instead of gas x gasPrice from transactions
    // Perhaps this has something to do with the ganache instance it spins up?
    "gasPrice": "0x00000001",
  },
};