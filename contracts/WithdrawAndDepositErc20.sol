// SPDX-License-Identifier: AGPL
pragma solidity ^0.8.6;

/*-
 * #%L
 * byoc-contract-eth
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import {IERC20, SafeERC20} from "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import {WithdrawAndDepositUpgradeable} from "./WithdrawAndDepositUpgradeable.sol";
import {LargeOracleInterface} from "./LargeOracleInterface.sol";

/// @title The BYOC contract for creating ETH-PBC pairs.
contract WithdrawAndDepositErc20 is WithdrawAndDepositUpgradeable {
    using SafeERC20 for IERC20;

    IERC20 public token;


    /// @param _depositMinimum the minimum amount that can be deposited
    /// @param _depositMaximum the maximum amount that can be deposited
    /// @param _oracles the addresses of the PBC oracles that approves withdraws (the small oracle)
    /// @param _largeOracleContract the migration oracle
    function initialize(IERC20 _token,
                        uint256 _depositMinimum,
                        uint256 _depositMaximum,
                        uint256 _withdrawMaximum,
                        address[] memory _oracles,
                        LargeOracleInterface _largeOracleContract) external initializer {
        token = _token;
        _initialize(_depositMinimum, _depositMaximum, _withdrawMaximum, _oracles, _largeOracleContract);
    }

    function deposit(bytes21 destination, uint256 amount) external {
        token.safeTransferFrom(msg.sender, address(this), amount);
        _deposit(destination, amount);
    }

    function _sendValue(address destination, uint256 amount) internal override {
        token.safeTransfer(destination, amount);
    }
}
