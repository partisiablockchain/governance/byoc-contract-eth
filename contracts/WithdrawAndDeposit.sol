// SPDX-License-Identifier: AGPL
pragma solidity ^0.8.6;

/*-
 * #%L
 * byoc-contract-eth
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import {ECDSA} from "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import {LargeOracleInterface} from  "./LargeOracleInterface.sol";
import {LargeOracleUpgradeable} from "./LargeOracleUpgradeable.sol";

/// @title The BYOC contract for creating ETH-PBC pairs.
abstract contract WithdrawAndDeposit is LargeOracleUpgradeable {

    event Deposit (
        uint64 nonce,
        bytes21 destination,
        uint256 amount
    );

    event Withdrawal (
        uint64 withdrawalNonce,
        address destination,
        uint256 amount,
        uint64 oracleNonce,
        uint32 bitmask
    );

    uint64 public depositNonce;
    uint256 public depositMinimum;
    uint256 public depositMaximum;

    uint256 public withdrawSum;
    uint256 public withdrawMaximum;

    address[] public oracles;

    LargeOracleInterface public largeOracle;
    uint64 public largeOracleNonce;
    uint64 public oracleNonce;

    /// Mapping from oracleNonce to merke root of valid withdrawals
    mapping(uint64 => bytes32) public epochs;
    /// Mapping from (oracleNonce, withdrawNonce) to keccak(oracleNonce, withdrawNonce, destination, amount)
    mapping(uint256 => bytes32) public processedWithdrawals;

    /// @param _depositMinimum the minimum amount that can be deposited
    /// @param _depositMaximum the maximum amount that can be deposited
    /// @param _oracles the addresses of the PBC oracles that approves withdraws (the small oracle)
    /// @param _largeOracleContract the migration oracle
    function _initialize(uint256 _depositMinimum,
        uint256 _depositMaximum,
        uint256 _withdrawMaximum,
        address[] memory _oracles,
        LargeOracleInterface _largeOracleContract) internal {
        depositNonce = 0;
        depositMinimum = _depositMinimum;
        depositMaximum = _depositMaximum;
        withdrawMaximum = _withdrawMaximum;
        withdrawSum = 0;
        oracles = _oracles;
        largeOracle = _largeOracleContract;
        largeOracleNonce = 0;
        oracleNonce = 0;
    }

    /// @notice Gives the number of oracles needed to approve a withdrawal
    function getOracleSize() external view returns (uint256) {
        return oracles.length;
    }

    /// @notice Update the address of the oracle performing validation of withdraws
    /// @param newLargeOracleNonce the nonce for this update request
    /// @param oracleKeys the public keys of the new small oracles
    /// @param signature a signature attesting to the validity of this update
    /// @param merkleTree the root of a merkle tree containing the hash of all transactions for the old epoch
    function update(uint64 newLargeOracleNonce,
        bytes calldata oracleKeys,
        bytes32 merkleTree,
        bytes calldata signature) external {

        // Updates must come from the current large oracle (this corresponds to the = case) or a
        // large oracle which we haven't seen yet (this would be the > case).
        require(newLargeOracleNonce >= largeOracleNonce, "Invalid nonce for oracle update");

        // Uncompressed size of a public key (EC point).
        uint256 keySize = 64;
        uint256 noOfKeys = oracleKeys.length / keySize;
        // Catch silly mistakes.
        require(noOfKeys > 0, "No oracles");

        bytes32 digest = sha256(abi.encodePacked(
            address(this),
            oracleNonce,
            merkleTree,
            oracleKeys));
        require(largeOracle.verifyUpdate(newLargeOracleNonce, digest, signature),
            "Could not verify signature");

        epochs[oracleNonce] = merkleTree;

        address[] memory newOracles = new address[](noOfKeys);
        for (uint256 i = 0; i < noOfKeys; i++) {
            bytes memory pubKey = oracleKeys[i * keySize : (i + 1) * keySize];
            address oracle = address(uint160(uint256(keccak256(pubKey))));
            newOracles[i] = oracle;
        }

        oracles = newOracles;
        oracleNonce += 1;
        largeOracleNonce = newLargeOracleNonce;
        withdrawSum = 0;
    }

    /// @notice Perform a deposit, making it possible to withdraw an equivalent amount on PBC.
    /// @param destination the PBC address where the deposit can be withdrawn
    function _deposit(bytes21 destination, uint256 amount) internal {
        require(depositMinimum <= amount && amount <= depositMaximum,
            "Invalid deposit amount");
        require(destination[0] == 0, "Invalid destination");

        uint64 currentNonce = depositNonce;
        depositNonce += 1;

        emit Deposit(currentNonce, destination, amount);
    }

    /// @notice Perform a withdraw of an amount of ETH
    /// @param destination the address where ETH is transferred
    /// @param amount the amount of ETH to be withdrawn
    /// @param bitmask a bitmask of which oracles authorized the withdraw
    /// @param signatures signatures attesting to the validity of the withdraw request
    function withdraw(uint64 withdrawNonce,
        address destination,
        uint256 amount,
        uint32 bitmask,
        bytes calldata signatures) external {
        require(withdrawSum + amount <= withdrawMaximum, "Unable to withdraw amount");

        // This is the default size of an ECDSA signature.
        uint256 sigSize = 65;

        bytes32 digest = sha256(abi.encodePacked(address(this), oracleNonce, withdrawNonce, destination, amount));

        uint256 sigs = 0;
        uint32 offset = 0;
        uint256 end = sigSize;
        uint32 signersBitmask = 0;
        while (end <= signatures.length) {
            bytes memory signature = signatures[end - sigSize : end];
            // figure out which oracle produced this signature.
            while (offset < oracles.length && (bitmask >> offset) & 1 == 0) {
                offset++;
            }
            // if we're at the end, then we stop parsing signatures.
            if (offset == oracles.length) {
                break;
            }
            require(oracles[offset] == ECDSA.recover(digest, signature),
                "Could not verify signature");

            signersBitmask |= uint32(1) << offset;
            offset++;
            sigs++;
            end += sigSize;
        }

        require(sigs >= (oracles.length / 2) + 1, "Not enough signatures");

        withdrawSum += amount;

        uint256 withdrawId = (uint256(oracleNonce) << 64) + uint256(withdrawNonce);
        require(processedWithdrawals[withdrawId] == 0, "Withdrawal already processed");
        bytes32 withdrawContent = keccak256(abi.encodePacked(oracleNonce, withdrawNonce, destination, amount));
        processedWithdrawals[withdrawId] = withdrawContent;
        emit Withdrawal(withdrawNonce, destination, amount, oracleNonce, signersBitmask);

        _sendValue(destination, amount);
    }

    /// @notice Perform an out of order withdraw of an amount of ETH from passed epoch
    /// @param nonce the nonce of the withdraw
    /// @param destination the address where the ETH is transferred
    /// @param amount the amount of ETH to be withdrawn
    /// @param epochOracleNonce the nonce of the oracle identifying the epoch in which the withdraw occurred
    /// @param proof merkle proof needed to prove that the transaction belongs to the specified epoch
    function withdrawFromPastEpoch(uint64 nonce,
        address destination,
        uint256 amount,
        uint64 epochOracleNonce,
        bytes32[] memory proof) external {
        bytes32 merkleRoot = epochs[epochOracleNonce];

        bytes32 digest = sha256(abi.encodePacked(nonce, destination, amount));
        require(verifyMerkleProof(proof, merkleRoot, digest),
            "Unable to verify proof");

        uint256 withdrawId = (uint256(epochOracleNonce) << 64) + uint256(nonce);
        bytes32 withdrawContent = keccak256(abi.encodePacked(epochOracleNonce, nonce, destination, amount));
        require(processedWithdrawals[withdrawId] != withdrawContent, "Withdrawal already processed");
        processedWithdrawals[withdrawId] = withdrawContent;
        emit Withdrawal(nonce, destination, amount, epochOracleNonce, 0);

        _sendValue(destination, amount);
    }

    function _sendValue(address destination, uint256 amount) internal virtual;

    // Based on @openzeppelin/contracts/utils/cryptography/MerkleProof.sol
    // This function asserts that the Merkle Tree has been build with sorted pairs
    function verifyMerkleProof(bytes32[] memory proof,
        bytes32 root,
        bytes32 leaf) private pure returns (bool) {
        bytes32 computedHash = leaf;

        for (uint256 i = 0; i < proof.length; i++) {
            bytes32 proofElement = proof[i];

            if (computedHash <= proofElement) {
                // Hash(current computed hash + current element of the proof)
                computedHash = sha256(abi.encode(computedHash, proofElement));
            } else {
                // Hash(current element of the proof + current computed hash)
                computedHash = sha256(abi.encode(proofElement, computedHash));
            }
        }

        // Check if the computed hash (root) is equal to the provided root
        return computedHash == root;
    }

    function _largeOracle() internal virtual view override returns (LargeOracleInterface) {
        return largeOracle;
    }
}
