// SPDX-License-Identifier: AGPL
pragma solidity ^0.8.6;

/*-
 * #%L
 * byoc-contract-eth
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

/**
 * Interface for accessing information about LargeOracle updates.
 */
abstract contract LargeOracleInterface {
    /// @param nonce the nonce associated update request
    /// @param digest the digest which was signed
    /// @param signature the signature
    function verifyUpdate(uint64 nonce, bytes32 digest, bytes calldata signature) public virtual view returns (bool);

    /// @notice Verify that the supplied signature is signed by the current large oracle
    /// @param digest the digest which was signed
    /// @param signature the signature
    function verifyContractUpdate(bytes32 digest, bytes calldata signature) public virtual view returns (bool);
}
