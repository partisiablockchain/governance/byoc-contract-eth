// SPDX-License-Identifier: AGPL
pragma solidity ^0.8.6;

/*-
 * #%L
 * byoc-contract-eth
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import {WithdrawAndDepositUpgradeable} from "./WithdrawAndDepositUpgradeable.sol";
import {LargeOracleInterface} from "./LargeOracleInterface.sol";

/// @title The BYOC contract for creating ETH-PBC pairs.
contract WithdrawAndDepositEth is WithdrawAndDepositUpgradeable {
    using Address for address payable;

    /// @param _depositMinimum the minimum amount that can be deposited
    /// @param _depositMaximum the maximum amount that can be deposited
    /// @param _oracles the addresses of the PBC oracles that approves withdraws (the small oracle)
    /// @param _largeOracleContract the migration oracle
    function initialize(uint256 _depositMinimum,
                        uint256 _depositMaximum,
                        uint256 _withdrawMaximum,
                        address[] memory _oracles,
                        LargeOracleInterface _largeOracleContract) external initializer {
        _initialize(_depositMinimum, _depositMaximum, _withdrawMaximum, _oracles, _largeOracleContract);
    }

    function deposit(bytes21 destination) external payable {
        _deposit(destination, msg.value);
    }

    function _sendValue(address destination, uint256 amount) internal override {
        payable(destination).sendValue(amount);
    }
}
