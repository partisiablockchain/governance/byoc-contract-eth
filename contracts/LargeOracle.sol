// SPDX-License-Identifier: AGPL
pragma solidity ^0.8.6;

/*-
 * #%L
 * byoc-contract-eth
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import {Initializable} from "@openzeppelin/contracts/proxy/utils/Initializable.sol";
import {ECDSA} from "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import {LargeOracleInterface} from "./LargeOracleInterface.sol";
import {LargeOracleUpgradeable} from "./LargeOracleUpgradeable.sol";

/// @title The contract responsible for verifying new small oracles
contract LargeOracle is LargeOracleInterface, LargeOracleUpgradeable, Initializable {

    struct Oracle {
        uint32 size;
        bytes32 merkleRoot;
        address oracle;
    }

    bytes32 private domainSeparator;

    uint64 public largeNonce;
    mapping(uint64 => Oracle) public oracleMap;

    // Keeps track of signers in the case of an exceptional update.
    mapping(uint64 => mapping(address => bool)) private exceptionalUpdates;
    mapping(bytes32 => uint32) private signatureCounters;

    constructor() {
        _disableInitializers();
    }

    /// @param _size the size of the initial oracle
    /// @param _merkleRoot merkle root for oracle members
    /// @param _oracle large oracle address
    function initialize(uint32 _size, bytes32 _merkleRoot, address _oracle, bytes32 _domainSeparator) external initializer {
        largeNonce = 0;
        oracleMap[largeNonce] = Oracle(_size, _merkleRoot, _oracle);
        domainSeparator = _domainSeparator;
    }

    /// @notice Updates the large oracle by presenting a signature from the current oracle
    /// @param newSize size of the new oracle
    /// @param newMerkleRoot root of the tree with new oracle nodes
    /// @param newOracle the new oracle
    /// @param signature a signature from the previous oracle on the new one
    function update(uint32 newSize, bytes32 newMerkleRoot, bytes calldata newOracle,
        bytes calldata signature) external {

        uint64 newLargeNonce = largeNonce + 1;
        bytes32 digest = computeDigest(newLargeNonce, newSize, newMerkleRoot, newOracle);
        require(verifyUpdate(largeNonce, digest, signature), "Invalid signature");
        executeUpdate(newLargeNonce, newSize, newMerkleRoot, newOracle);
    }

    /// @notice provides a means to update the large oracle in case the threshold key is broken
    /// @param newSize size of the new oracle
    /// @param newMerkleRoot root of the tree with new oracle nodes
    /// @param newOracle the new oracle
    /// @param signerPbcKey the uncompressed key of the signer
    /// @param proof a proof that the caller is part of the current oracle
    /// @param signature signature by caller on the new oracle
    function updateExceptional(
        uint32 newSize,
        bytes32 newMerkleRoot,
        bytes calldata newOracle,
        bytes calldata signerPbcKey,
        bytes32[] memory proof,
        bytes calldata signature)
    external {
        uint64 newLargeNonce = largeNonce + 1;
        bytes32 digest = computeDigest(newLargeNonce, newSize, newMerkleRoot, newOracle);
        address signer = ECDSA.recover(digest, signature);
        require(keyToAddress(signerPbcKey) == signer, "Incorrect key provided");

        // A particular oracle node is only allowed to "vote" on one oracle update per nonce. When
        // a particular update for this nonce gets 2t+1 votes, then that oracle becomes the new one.
        require(!exceptionalUpdates[newLargeNonce][signer], "Signer already signed this nonce");
        exceptionalUpdates[newLargeNonce][signer] = true;
        Oracle memory currentOracle = oracleMap[largeNonce];
        require(verifyMerkleProof(proof, currentOracle.merkleRoot, signerPbcKey),
            "Signer not part of oracle");

        // initial value is guaranteed to be zero, so this makes sense.
        signatureCounters[digest] += 1;

        // this threshold could probably just be t+1.
        if (signatureCounters[digest] > 2 * (currentOracle.size / 3)) {
            executeUpdate(newLargeNonce, newSize, newMerkleRoot, newOracle);
        }
    }

    // Based on @openzeppelin/contracts/utils/cryptography/MerkleProof.sol
    // This function asserts that the Merkle Tree has been build with sorted pairs
    // Slight adaption of code in the WithdrawAndDeposit contract
    function verifyMerkleProof(bytes32[] memory proof,
        bytes32 root,
        bytes calldata signer) private pure returns (bool) {
        bytes32 computedHash = sha256(abi.encodePacked(signer));

        for (uint256 i = 0; i < proof.length; i++) {
            bytes32 proofElement = proof[i];

            if (computedHash <= proofElement) {
                // Hash(current computed hash + current element of the proof)
                computedHash = sha256(abi.encode(computedHash, proofElement));
            } else {
                // Hash(current element of the proof + current computed hash)
                computedHash = sha256(abi.encode(proofElement, computedHash));
            }
        }

        // Check if the computed hash (root) is equal to the provided root
        return computedHash == root;
    }

    /// @param nonce the nonce associated update request
    /// @param digest the digest which was signed
    /// @param signature the signature
    function verifyUpdate(uint64 nonce, bytes32 digest, bytes calldata signature)
    public view override returns (bool) {
        return oracleMap[nonce].oracle == ECDSA.recover(digest, signature);
    }

    /// @notice Verify that the supplied signature is signed by the current large oracle
    /// @param digest the digest which was signed
    /// @param signature the signature
    function verifyContractUpdate(bytes32 digest, bytes calldata signature)
    public view override returns (bool) {
        return verifyUpdate(largeNonce, digest, signature);
    }

    /// @notice Update the large oracle mapping. Called when an oracle update has been validated
    function executeUpdate(uint64 newLargeNonce, uint32 size, bytes32 merkleRoot,
        bytes calldata oracle)
    private {
        oracleMap[newLargeNonce] = Oracle(
            size,
            merkleRoot,
            keyToAddress(oracle)
        );
        largeNonce = newLargeNonce;
    }

    /// @notice Compute the digest that the large oracle signs when updating to a new large oracle
    function computeDigest(uint64 nonce, uint32 size, bytes32 merkleRoot, bytes calldata oracle)
    private view returns (bytes32) {
        return sha256(abi.encodePacked(domainSeparator, nonce, size, merkleRoot, oracle));
    }

    /// @notice convert an off-chain key to an ETH address
    function keyToAddress(bytes calldata key) private pure returns (address) {
        return address(uint160(uint256(keccak256(key))));
    }

    function _largeOracle() internal virtual view override returns (LargeOracleInterface) {
        return LargeOracleInterface(address(this));
    }
}
