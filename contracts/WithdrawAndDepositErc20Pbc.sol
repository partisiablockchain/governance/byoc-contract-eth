// SPDX-License-Identifier: AGPL
pragma solidity ^0.8.6;

/*-
 * #%L
 * byoc-contract-eth
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

import {WithdrawAndDeposit} from "./WithdrawAndDeposit.sol";
import {ERC20Upgradeable} from "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import {LargeOracleInterface} from "./LargeOracleInterface.sol";

/// @title The contract for bridging PBC-BYOC to ERC20
contract WithdrawAndDepositErc20Pbc is WithdrawAndDeposit, ERC20Upgradeable {

    uint8 private decimals_;

    /// @param _depositMinimum the minimum amount that can be deposited
    /// @param _depositMaximum the maximum amount that can be deposited
    /// @param _oracles the addresses of the PBC oracles that approves withdraws (the small oracle)
    /// @param _largeOracleContract the migration oracle
    /// @param _name the name of the token
    /// @param _symbol the symbol of the token
    function initialize(
        uint256 _depositMinimum,
        uint256 _depositMaximum,
        uint256 _withdrawMaximum,
        address[] memory _oracles,
        LargeOracleInterface _largeOracleContract,
        string memory _name,
        string memory _symbol,
        uint8 _decimals) external initializer {
        __ERC20_init(_name, _symbol);
        decimals_ = _decimals;
        _initialize(_depositMinimum, _depositMaximum, _withdrawMaximum, _oracles, _largeOracleContract);
    }

    constructor() {
        _disableInitializers();
    }

    function decimals() public view override returns (uint8) {
        return decimals_;
    }

    function deposit(bytes21 destination, uint256 amount) external {
        _burn(msg.sender, amount);
        _deposit(destination, amount);
    }

    function _sendValue(address destination, uint256 amount) internal override {
        _mint(destination, amount);
    }
}
