const WithdrawAndDepositErc20 = artifacts.require("WithdrawAndDepositErc20");
const LargeOracle = artifacts.require("LargeOracle");
const ERC1967Proxy = artifacts.require("ERC1967Proxy");
const ERC20PresetFixedSupply = artifacts.require(
    "@openzeppelin/contracts/token/ERC20/presets/ERC20PresetFixedSupply.sol");
const truffleAssert = require("truffle-assertions");
const hash = require("hash.js");
const BN = require("bn.js");
const ethUtil = require("ethereumjs-util");
const {MerkleTree} = require("merkletreejs");

contract("WithdrawAndDepositErc20", accounts => {
  let contract;
  let contractImplementation;
  let largeOracleContract;
  let largeOracleImplementation;
  let oracles = [];
  let oraclePrvKeys = [];
  let largeOracle;
  let largeOracleMerkleRoot;
  let largeOraclePrvKey;
  let largeOraclePubKey;
  let largeOracleNodeKeys = [];
  let largeOracleNodePubKeys = [];
  let largeOracleNodes = [];
  let token;
//  let domainSeparator = hash.sha256().update("Partisiablockchain").digest("hex");
  let domainSeparator = "5f02a327a959a08bee16235910ad712c36507992cb310c9e064c96f27d92a3e7";
  // It is necessary to not use too big numbers, since apparently the sender is reused during the
  // entire test suite, so it may run out of gas before all tests have finished. Figure out if
  // there is some way to increase gas for truffle test sender.
  const minDeposit = web3.utils.toWei(new BN("1000000"), "gwei"); // 0.001 eth
  const maxDeposit = web3.utils.toWei(new BN("100000000"), "gwei"); // 0.1 eth
  const maxWithdraw = web3.utils.toWei(new BN("500000000"), "gwei"); // 0.5 eth
  const pbcAccount = "0x00339896577caf2fc1cdcb1b144683e1312b791c24";

  function initializeLargeOracle() {
    largeOracle = web3.eth.accounts.create();
    largeOraclePrvKey = Buffer.from(largeOracle.privateKey.substring(2), "hex");
    largeOraclePubKey = ethUtil.privateToPublic(largeOraclePrvKey);
    for (let i = 0; i < 4; i++) {
      const node = web3.eth.accounts.create();
      largeOracleNodes[i] = node;
      largeOracleNodeKeys[i] = Buffer.from(node.privateKey.substring(2), "hex");
      largeOracleNodePubKeys[i] = ethUtil.privateToPublic(largeOracleNodeKeys[i]);
    }
    largeOracleMerkleRoot = createLargeOracleMerkleTree(largeOracleNodes);
  }

  beforeEach(async () => {
    let tokenAmount = web3.utils.toWei(new BN(10), "ether");
    token = await ERC20PresetFixedSupply.new(
        "MyToken",
        "MyToken",
        tokenAmount,
        accounts[0]);
    oracles[0] = web3.eth.accounts.create();
    oracles[1] = web3.eth.accounts.create();
    oracles[2] = web3.eth.accounts.create();
    oraclePrvKeys[0] = Buffer.from(oracles[0].privateKey.substring(2), "hex");
    oraclePrvKeys[1] = Buffer.from(oracles[1].privateKey.substring(2), "hex");
    oraclePrvKeys[2] = Buffer.from(oracles[2].privateKey.substring(2), "hex");
    initializeLargeOracle();
    largeOracleImplementation = await LargeOracle.new();
    const largeOracleInit = largeOracleImplementation.contract.methods.initialize(4,
        largeOracleMerkleRoot.getHexRoot(),
        largeOracle.address, Buffer.from(domainSeparator, "hex")).encodeABI();
    const largeOracleContractProxy = await ERC1967Proxy.new(largeOracleImplementation.address,
        largeOracleInit);
    largeOracleContract = await LargeOracle.at(largeOracleContractProxy.address);
    contractImplementation = await WithdrawAndDepositErc20.new();
    const init = contractImplementation.contract.methods.initialize(token.address, minDeposit,
        maxDeposit,
        maxWithdraw,
        oracles.map(({address}) => address),
        largeOracleContract.address).encodeABI();
    const contractProxy = await ERC1967Proxy.new(contractImplementation.address, init);
    contract = await WithdrawAndDepositErc20.at(contractProxy.address);

    await token.approve(contract.address, tokenAmount);
  });

  it("minDeposit is a valid amount", async () => {
    await assertValidDeposit(minDeposit);
  });

  it("maxDeposit is a valid amount", async () => {
    await assertValidDeposit(maxDeposit);
  });

  it("deposit amount between max and min is valid", async () => {
    const depositAmount = new BN("49500000000000000");
    await assertValidDeposit(depositAmount);
  });

  async function assertValidDeposit(depositAmount) {
    const tx = await contract.deposit(pbcAccount, depositAmount);

    const {logs} = tx;
    assert.ok(Array.isArray(logs));
    assert.equal(logs.length, 1);

    const log = logs[0];
    assert.equal(log.event, "Deposit");
    assert.equal(log.args.nonce, 0);
    assert.equal(log.args.destination, pbcAccount);
    assert.equal(log.args.amount.toString(10), depositAmount.toString(10));
    let balance = await token.balanceOf(contract.address);
    assert.equal(balance, depositAmount.toString(10));
  }

  it("invalid deposit amounts", async () => {
    await truffleAssert.reverts(contract.deposit(pbcAccount, minDeposit.sub(new BN(1))),
        "Invalid deposit amount");
    await truffleAssert.reverts(contract.deposit(pbcAccount, maxDeposit.add(new BN(1))),
        "Invalid deposit amount");
  });
  it("invalid destination", async () => {
    const contractAccount = "0x01339896577caf2fc1cdcb1b144683e1312b791c24";
    await truffleAssert.reverts(contract.deposit(contractAccount, minDeposit),
        "Invalid destination");
  });

  it("consequent deposit bumps nonce", async () => {
    const tx0 = await contract.deposit(pbcAccount, minDeposit);
    const tx1 = await contract.deposit(pbcAccount, minDeposit);
    const tx2 = await contract.deposit(pbcAccount, minDeposit);

    assertNonce(tx0, 0);
    assertNonce(tx1, 1);
    assertNonce(tx2, 2);
  });

  function hashAddress(sha, address) {
    sha.update(web3.utils.hexToBytes(address));
  }

  function hashMerkleTree(sha, merkleTree) {
    const tree = web3.utils.padLeft(merkleTree, 64);
    sha.update(web3.utils.hexToBytes(tree));
  }

  function hashNumber(sha, number, size = 64) {
    const n = web3.utils.padLeft(web3.utils.numberToHex(number), size);
    sha.update(web3.utils.hexToBytes(n));
  }

  function messageDigestWithdraw(oracleNonce, nonce, destAddress, withdraw) {
    let sha = hash.sha256();

    hashAddress(sha, contract.address);
    hashNumber(sha, oracleNonce, 16);
    hashNumber(sha, nonce, 16);
    hashAddress(sha, destAddress);
    hashNumber(sha, withdraw);

    return Buffer.from(sha.digest("hex"), "hex");
  }

  function messageDigestOutOfOrderWithdraw(nonce, destAddress, amount) {
    let sha = hash.sha256();

    hashNumber(sha, nonce, 16);
    sha.update(web3.utils.hexToBytes(destAddress));
    hashNumber(sha, amount);

    return Buffer.from(sha.digest("hex"), "hex");
  }

  function sign(msgDigest, toSkip) {
    const sigs = [];
    if (toSkip === undefined) {
      toSkip = [];
    }
    for (let i = 0; i < oraclePrvKeys.length; i++) {
      if (toSkip.includes(i)) {
        continue;
      }
      const oraclePrvKey = oraclePrvKeys[i];
      const sig = ethUtil.ecsign(msgDigest, oraclePrvKey);
      sigs.push(Buffer.from(ethUtil.toRpcSig(sig.v, sig.r, sig.s).substring(2), "hex"));
    }
    return Buffer.concat(sigs);
  }

  function messageDigestUpdate(nonce, publicKeys, merkleTreeRoot) {
    let sha = hash.sha256();

    sha.update(web3.utils.hexToBytes(contract.address));
    hashNumber(sha, nonce, 16);
    hashMerkleTree(sha, merkleTreeRoot);
    sha.update(publicKeys);

    return Buffer.from(sha.digest("hex"), "hex");
  }

  function messageDigestLargeOracleUpdate(nonce, oracle) {
    let sha = hash.sha256();
    sha.update(Buffer.from(domainSeparator, "hex"));
    hashNumber(sha, nonce, 16);
    hashNumber(sha, oracle.size, 8);
    hashMerkleTree(sha, oracle.merkleRoot);
    sha.update(oracle.oraclePubKey);
    return Buffer.from(sha.digest("hex"), "hex");
  }

  function signWithLargeOracle(msgDigest) {
    return signWith(msgDigest, largeOraclePrvKey);
  }

  it("can withdraw withdrawMaximum", async () => {
    await contract.deposit(pbcAccount, maxDeposit);
    await contract.deposit(pbcAccount, maxDeposit);
    await contract.deposit(pbcAccount, maxDeposit);
    await contract.deposit(pbcAccount, maxDeposit);
    await contract.deposit(pbcAccount, maxDeposit);

    let destAddress = accounts[0];
    let initialBalance = new BN(await token.balanceOf(destAddress));
    let msgDigest = messageDigestWithdraw(0, 1, destAddress, maxWithdraw);
    let signatures = sign(msgDigest);

    let sum = await contract.withdrawSum();
    assert.equal(sum, 0);

    let result = await contract.withdraw(1, destAddress, maxWithdraw, 0b111, signatures);

    await assertWithdrawalLogEvent(result, 1, destAddress, maxWithdraw, 0, 0b111);

    sum = await contract.withdrawSum();
    assert.equal(sum.toString(10), maxWithdraw.toString(10));

    let balance = await token.balanceOf(destAddress);
    let expectedBalance = initialBalance.add(maxWithdraw);
    assert.equal(balance, expectedBalance.toString(10));
  });

  async function assertWithdrawalLogEvent(tx, nonce, receiver, amount, oracleNonce, bitmask) {
    const {logs} = tx;
    assert.ok(Array.isArray(logs));
    assert.equal(logs.length, 1);

    const log = logs[0];
    assert.equal(log.event, "Withdrawal");
    assert.equal(log.args.withdrawalNonce, nonce);
    assert.equal(log.args.destination, receiver);
    assert.equal(log.args.amount.toString(10), amount.toString(10));
    assert.equal(log.args.oracleNonce, oracleNonce);
    assert.equal(log.args.bitmask, bitmask);
  }

  it("can withdraw with 2 out of 3 signatures", async () => {
    await contract.deposit(pbcAccount, maxDeposit);

    let destAddress = accounts[0];
    let msgDigest0 = messageDigestWithdraw(0, 0, destAddress, minDeposit);
    let signatures0 = sign(msgDigest0, [0]);
    // Bitmask 110
    let result = await contract.withdraw(0, destAddress, minDeposit, 0b110, signatures0);
    await assertWithdrawalLogEvent(result, 0, destAddress, minDeposit, 0, 0b110);

    let msgDigest1 = messageDigestWithdraw(0, 1, destAddress, minDeposit);
    let signatures1 = sign(msgDigest1, [1]);
    // Bitmask 101
    result = await contract.withdraw(1, destAddress, minDeposit, 0b101, signatures1);
    await assertWithdrawalLogEvent(result, 1, destAddress, minDeposit, 0, 0b101);

    let msgDigest2 = messageDigestWithdraw(0, 2, destAddress, minDeposit);
    let signatures2 = sign(msgDigest2, [2]);
    // Bitmask 011
    result = await contract.withdraw(2, destAddress, minDeposit, 0b011, signatures2);
    await assertWithdrawalLogEvent(result, 2, destAddress, minDeposit, 0, 0b011);

    let sum = await contract.withdrawSum();
    let expectedSum = minDeposit.mul(new BN(3));
    assert.equal(sum.toString(10), expectedSum.toString(10));
  });

  it("withdraw signature validation breaks off when reaching oracles.length", async () => {
    await contract.deposit(pbcAccount, maxDeposit);

    let destAddress = accounts[0];
    let msgDigest = messageDigestWithdraw(0, 0, destAddress, minDeposit);
    oracles[3] = web3.eth.accounts.create();
    oraclePrvKeys[3] = Buffer.from(oracles[3].privateKey.substring(2), "hex");
    let signatures = sign(msgDigest, [0, 1]);
    await truffleAssert.reverts(
        // Bitmask 1100 (but only 3 known oracles, so not enough signatures from known oracles)
        contract.withdraw(0, destAddress, minDeposit, 0b1100, signatures),
        "Not enough signatures");
  });

  it("cannot withdraw more than withdrawMaximum", async () => {
    await contract.deposit(pbcAccount, maxDeposit);
    await contract.deposit(pbcAccount, maxDeposit);
    await contract.deposit(pbcAccount, maxDeposit);
    await contract.deposit(pbcAccount, maxDeposit);
    await contract.deposit(pbcAccount, maxDeposit);
    await contract.deposit(pbcAccount, minDeposit);

    let destAddress = accounts[0];
    let withdrawAmount = maxWithdraw.add(new BN(1));
    let msgDigest = messageDigestWithdraw(0, 0, destAddress, withdrawAmount);
    let signatures = sign(msgDigest);

    await truffleAssert.reverts(
        contract.withdraw(0, destAddress, withdrawAmount, 0b111, signatures),
        "Unable to withdraw amount");
  });

  it("cannot withdraw twice", async () => {
    await contract.deposit(pbcAccount, maxDeposit);

    let destAddress = accounts[0];
    let withdrawAmount = minDeposit;
    let msgDigest = messageDigestWithdraw(0, 0, destAddress, withdrawAmount);
    let signatures = sign(msgDigest);

    await contract.withdraw(0, destAddress, withdrawAmount, 0b111, signatures);

    await truffleAssert.reverts(
        contract.withdraw(0, destAddress, withdrawAmount, 0b111, signatures),
        "Withdrawal already processed");
  });

  it("withdrawal reverts if transfer fails", async () => {
    let msgDigest = messageDigestWithdraw(0, 0, accounts[1], minDeposit);
    let signatures = sign(msgDigest);

    await truffleAssert.reverts(
        contract.withdraw(0, accounts[1], minDeposit, 0b111, signatures),
        "ERC20: transfer amount exceeds balance.");
  });

  it("cannot withdraw with wrong index", async () => {
    let destAddress = accounts[0];
    let msgDigest = messageDigestWithdraw(0, 0, accounts[1], 10);
    let signature = sign(msgDigest, [2]);

    await truffleAssert.reverts(
        // Mask should be 011
        contract.withdraw(0, destAddress, 10, 0b110, signature),
        "Could not verify signature");
  });

  function merkleTreeSha256(data) {
    return Buffer.from(hash.sha256().update(data).digest("hex"), "hex");
  }

  function createMerkleTree(leaves) {
    return new MerkleTree(leaves, merkleTreeSha256, {sortPairs: true});
  }

  function createLargeOracleMerkleTree(nodes) {
    const hashed = [];
    for (let i = 0; i < nodes.length; i++) {
      let prv = Buffer.from(nodes[i].privateKey.substring(2), "hex");
      let sha = hash.sha256();
      sha.update(ethUtil.privateToPublic(prv));
      hashed[i] = Buffer.from(sha.digest("hex"), "hex");
    }
    return new MerkleTree(hashed, merkleTreeSha256, {sortPairs: true, sortLeaves: true});
  }

  async function updateSmallOracle(merkleTreeRoot) {
    let currentNonce = parseInt(await contract.oracleNonce());
    let currentLargeOracleNonce = parseInt(await contract.largeOracleNonce());

    let newOracle0 = web3.eth.accounts.create();
    let prvKey0 = Buffer.from(newOracle0.privateKey.substring(2), "hex");
    let pubKey0 = ethUtil.privateToPublic(prvKey0);
    let newOracle1 = web3.eth.accounts.create();
    let prvKey1 = Buffer.from(newOracle1.privateKey.substring(2), "hex");
    let pubKey1 = ethUtil.privateToPublic(prvKey1);
    let newOracle2 = web3.eth.accounts.create();
    let prvKey2 = Buffer.from(newOracle2.privateKey.substring(2), "hex");
    let pubKey2 = ethUtil.privateToPublic(prvKey2);
    let publicKeys = Buffer.concat([pubKey0, pubKey1, pubKey2]);

    let msg = messageDigestUpdate(
        currentNonce,
        publicKeys,
        merkleTreeRoot);
    let signature = signWithLargeOracle(msg);

    await contract.update(
        currentLargeOracleNonce,
        publicKeys,
        merkleTreeRoot,
        signature);
  }

  it("out-of-order withdraw from previous epoch", async () => {
    let account0 = accounts[0];
    let account1 = accounts[1];
    await contract.deposit(pbcAccount, maxDeposit);

    let pbcNonce = 0;
    let withdraw0 = messageDigestOutOfOrderWithdraw(pbcNonce++, account0, 250);
    let withdraw1 = messageDigestOutOfOrderWithdraw(pbcNonce++, account1, 495);
    let withdraw2 = messageDigestOutOfOrderWithdraw(pbcNonce++, account0, 150);

    let merkleTree = createMerkleTree([withdraw0, withdraw1, withdraw2]);

    await updateSmallOracle(merkleTree.getHexRoot());

    let proof0 = merkleTree.getHexProof(withdraw0);
    let proof1 = merkleTree.getHexProof(withdraw1);
    let proof2 = merkleTree.getHexProof(withdraw2);
    let tx1 = await contract.withdrawFromPastEpoch(1, accounts[1], 495, 0, proof1);
    let tx0 = await contract.withdrawFromPastEpoch(0, accounts[0], 250, 0, proof0);
    let tx2 = await contract.withdrawFromPastEpoch(2, accounts[0], 150, 0, proof2);
    await assertWithdrawalLogEvent(tx0, 0, accounts[0], 250, 0, 0b000);
    await assertWithdrawalLogEvent(tx1, 1, accounts[1], 495, 0, 0b000);
    await assertWithdrawalLogEvent(tx2, 2, accounts[0], 150, 0, 0b000);
  });

  it("cannot out-of-order withdraw twice", async () => {
    await contract.deposit(pbcAccount, maxDeposit);

    let withdraw = messageDigestOutOfOrderWithdraw(0, accounts[0], 250);
    let merkleTree = createMerkleTree([withdraw]);

    await updateSmallOracle(merkleTree.getHexRoot());

    let proof = merkleTree.getHexProof(withdraw);
    await contract.withdrawFromPastEpoch(0, accounts[0], 250, 0, proof);
    await truffleAssert.reverts(
        contract.withdrawFromPastEpoch(0, accounts[0], 250, 0, proof),
        "Withdrawal already processed");
  });

  it("cannot out-of-order withdraw from an epoch that has not yet occured", async () => {
    let withdraw = messageDigestOutOfOrderWithdraw(0, accounts[0], 250);
    let merkleTree = createMerkleTree([withdraw]);

    let proof = merkleTree.getHexProof(withdraw);
    await truffleAssert.reverts(
        contract.withdrawFromPastEpoch(0, accounts[0], 250, 0, proof),
        "Unable to verify proof"); // Since it is not possible to produce a valid merkle proof.
  });

  it("cannot out-of-order withdraw if already done so", async () => {
    await contract.deposit(pbcAccount, maxDeposit);
    let destAddress = accounts[0];
    let msgDigest = messageDigestWithdraw(0, 0, destAddress, 10);
    let signatures = sign(msgDigest);

    await contract.withdraw(0, destAddress, 10, 0b111, signatures);
    let withdraw = messageDigestOutOfOrderWithdraw(0, destAddress, 10);

    let merkleTree = createMerkleTree([withdraw]);
    let proof = merkleTree.getHexProof(withdraw);

    await updateSmallOracle(merkleTree.getHexRoot());
    await truffleAssert.reverts(
        contract.withdrawFromPastEpoch(0, destAddress, 10, 0, proof),
        "Withdrawal already processed");
  });

  it("can withdraw out-of-order if the one processed in order was malicious", async () => {
    await contract.deposit(pbcAccount, maxDeposit);
    let destAddress = accounts[0];
    let msgDigest = messageDigestWithdraw(0, 0, destAddress, 10);
    let signatures = sign(msgDigest);

    await contract.withdraw(0, destAddress, 10, 0b111, signatures);
    let withdraw = messageDigestOutOfOrderWithdraw(0, destAddress, 250);

    let merkleTree = createMerkleTree([withdraw]);
    let proof = merkleTree.getHexProof(withdraw);

    await updateSmallOracle(merkleTree.getHexRoot());
    await contract.withdrawFromPastEpoch(0, destAddress, 250, 0, proof);
  });

  it("cannot out-of-order withdraw with invalid proof", async () => {
    await contract.deposit(pbcAccount, maxDeposit);

    let destAddress = accounts[0];
    let withdraw0 = messageDigestOutOfOrderWithdraw(0, destAddress, 250);
    let withdraw1 = messageDigestOutOfOrderWithdraw(1, destAddress, 500);
    let badWithdraw0 = messageDigestOutOfOrderWithdraw(0, accounts[1], 1337);
    let badWithdraw1 = messageDigestOutOfOrderWithdraw(0, accounts[1], 4242);

    let merkleTree = createMerkleTree([withdraw0, withdraw1]);
    let badTree = createMerkleTree([badWithdraw0, badWithdraw1]);
    let badProof = badTree.getHexProof(badWithdraw1);

    await updateSmallOracle(merkleTree.getHexRoot());
    await truffleAssert.reverts(
        contract.withdrawFromPastEpoch(1, destAddress, 250, 0, badProof),
        "Unable to verify proof");
  });

  it("out-of-order withdrawal reverts if transfer fails", async () => {
    let withdraw = messageDigestOutOfOrderWithdraw(0, accounts[1], 250);
    let withdraw1 = messageDigestOutOfOrderWithdraw(1, accounts[1], 500);

    let merkleTree = createMerkleTree([withdraw, withdraw1]);
    let proof = merkleTree.getHexProof(withdraw1);

    await updateSmallOracle(merkleTree.getHexRoot());

    await truffleAssert.reverts(
        contract.withdrawFromPastEpoch(1, accounts[1], 500, 0, proof),
        "ERC20: transfer amount exceeds balance.");
  });

  it("update oracle succeeds", async () => {
    let currentNonce = parseInt(await contract.oracleNonce());
    let currentLargeOracleNonce = parseInt(await contract.largeOracleNonce());
    let numberOfOracles = parseInt(await contract.getOracleSize());
    let currentOracle0 = await contract.oracles(0);
    let currentOracle1 = await contract.oracles(1);
    let currentOracle2 = await contract.oracles(2);
    assert.equal(currentLargeOracleNonce, 0);
    assert.equal(numberOfOracles, oracles.length);
    assert.equal(currentNonce, 0);
    assert.equal(currentOracle0, oracles[0].address);
    assert.equal(currentOracle1, oracles[1].address);
    assert.equal(currentOracle2, oracles[2].address);

    let newOracle = web3.eth.accounts.create();
    let prvKey = Buffer.from(newOracle.privateKey.substring(2), "hex");
    let pubKey = ethUtil.privateToPublic(prvKey);

    let emptyMerkleTree = createMerkleTree([]).getHexRoot();
    let msg = messageDigestUpdate(
        currentNonce,
        pubKey,
        emptyMerkleTree);

    let signature = signWithLargeOracle(msg);

    await contract.update(
        currentLargeOracleNonce,
        pubKey,
        emptyMerkleTree,
        signature);

    let updatedOracleSize = parseInt(await contract.getOracleSize());
    let updatedLargeOracleNonce = parseInt(await contract.largeOracleNonce());
    let updatedNonce = parseInt(await contract.oracleNonce());
    let updatedOracle = await contract.oracles(0);
    // large oracle still the same
    assert.equal(updatedLargeOracleNonce, currentLargeOracleNonce);
    // only have 1 oracle now
    assert.equal(updatedOracleSize, 1);
    assert.equal(updatedOracle, newOracle.address);
    // + 1 for small oracle updates
    assert.equal(1 + currentNonce, updatedNonce);
  });

  it("update oracle contract succeeds", async () => {
    let sha = hash.sha256();

    sha.update(web3.utils.hexToBytes(contract.address));
    sha.update(web3.utils.hexToBytes(contractImplementation.address));
    sha.update(web3.utils.hexToBytes(contractImplementation.address));
    const message = Buffer.from(sha.digest("hex"), "hex");

    const signature = signWithLargeOracle(message);
    await contract.upgradeTo(signature, contractImplementation.address);
  });

  it("update large oracle contract succeeds", async () => {
    const message = contractUpdateMessage();

    const signature = signWithLargeOracle(message);
    await largeOracleContract.upgradeTo(signature, largeOracleImplementation.address);
  });

  it("update large oracle contract fails with wrong signature", async () => {
    const message = contractUpdateMessage();
    const signature = signWith(message, oraclePrvKeys[0]);
    await truffleAssert.reverts(
        largeOracleContract.upgradeTo(signature, largeOracleImplementation.address),
        "Invalid signature");
  });

  function contractUpdateMessage() {
    let sha = hash.sha256();
    sha.update(web3.utils.hexToBytes(largeOracleContract.address));
    sha.update(web3.utils.hexToBytes(largeOracleImplementation.address));
    sha.update(web3.utils.hexToBytes(largeOracleImplementation.address));
    return Buffer.from(sha.digest("hex"), "hex");
  }

  function createNewOracle() {
    let nodes = [];
    for (let i = 0; i < 4; i++) {
      nodes[i] = web3.eth.accounts.create();
    }
    return nodes;
  }

  async function updateLargeOracle() {
    let newOracle = web3.eth.accounts.create();
    let currentNonce = parseInt(await largeOracleContract.largeNonce());
    let newNodes = createNewOracle();
    let merkleTree = createLargeOracleMerkleTree(newNodes);
    let oraclePubKey = ethUtil.privateToPublic(
        Buffer.from(newOracle.privateKey.substring(2), "hex"));
    let msg = messageDigestLargeOracleUpdate(currentNonce + 1,
        {size: 4, merkleRoot: merkleTree.getHexRoot(), oraclePubKey: oraclePubKey});
    let signature = signWithLargeOracle(msg);
    await largeOracleContract.update(4, merkleTree.getHexRoot(), oraclePubKey,
        signature);
    largeOracle = newOracle;
    largeOraclePrvKey = Buffer.from(largeOracle.privateKey.substring(2), "hex");
    largeOracleMerkleRoot = merkleTree;
  }

  it("cannot apply updates out-of-order", async () => {
    const largeOracleNonce = parseInt(await contract.largeOracleNonce());
    let emptyMerkleTree = createMerkleTree([]).getHexRoot();

    // valid update o0 -> o1 signed by large oracle 0
    const o1 = web3.eth.accounts.create();
    let pubKey1 = ethUtil.privateToPublic(Buffer.from(o1.privateKey.substring(2), "hex"));
    const m1 = messageDigestUpdate(
        0,
        pubKey1,
        emptyMerkleTree);
    const s1 = signWithLargeOracle(m1);

    await updateLargeOracle();

    // valid update o0 -> o2 signed by large oracle 1
    const o2 = web3.eth.accounts.create();
    let pubKey2 = ethUtil.privateToPublic(Buffer.from(o2.privateKey.substring(2), "hex"));
    const m2 = messageDigestUpdate(
        0,
        pubKey2,
        emptyMerkleTree);
    const s2 = signWithLargeOracle(m2);

    // We can update o0 -> o1 -> o2, but not o0 -> o2 -> o1.
    await contract.update(1, pubKey2, emptyMerkleTree, s2);
    const newLargeOracleNonce = parseInt(await contract.largeOracleNonce());
    assert.notEqual(largeOracleNonce, newLargeOracleNonce);
    assert.equal(newLargeOracleNonce, 1);

    await truffleAssert.reverts(
        contract.update(0, pubKey1, emptyMerkleTree, s1),
        "Invalid nonce for oracle update");
  });

  it("update fails without oracles", async () => {
    let emptyMerkleTree = createMerkleTree([]).getRoot();
    let signature = sign(messageDigestWithdraw(0, 0, accounts[0], 0));
    await truffleAssert.reverts(
        contract.update(1, [], emptyMerkleTree, signature),
        "No oracles");
  });

  it("update fails with invalid large oracle signature", async () => {
    const o1 = web3.eth.accounts.create();
    let pubKey1 = ethUtil.privateToPublic(Buffer.from(o1.privateKey.substring(2), "hex"));
    let emptyMerkleTree = createMerkleTree([]).getHexRoot();
    const m1 = messageDigestUpdate(
        0,
        pubKey1,
        emptyMerkleTree);
    const s1 = signWithLargeOracle(m1);
    const otherOracle = web3.eth.accounts.create();
    let pubKey2 = ethUtil.privateToPublic(
        Buffer.from(otherOracle.privateKey.substring(2), "hex"));
    await truffleAssert.reverts(
        contract.update(0, pubKey2, emptyMerkleTree, s1));
  });

  it("large oracle update fails with invalid nonce", async () => {
    const newOracle = web3.eth.accounts.create();
    const newNodes = createNewOracle();
    const newTree = createLargeOracleMerkleTree(newNodes).getHexRoot();
    const msg = messageDigestLargeOracleUpdate(0,
        {size: 4, merkleRoot: newTree, oracle: newOracle.address});
    const signature = signWithLargeOracle(msg);
    await truffleAssert.reverts(
        largeOracleContract.update(4, newTree, newOracle.address, signature));
  });

  it("large oracle update fails with invalid signature", async () => {
    const newOracle = web3.eth.accounts.create();
    const newNodes = createNewOracle();
    let merkleRoot = createLargeOracleMerkleTree(newNodes).getHexRoot();
    const msg = messageDigestLargeOracleUpdate(1,
        {size: 4, merkleRoot: merkleRoot, oracle: newOracle.address});
    const signature = signWithLargeOracle(msg);
    const otherOracle = web3.eth.accounts.create();
    await truffleAssert.reverts(
        largeOracleContract.update(4, merkleRoot, otherOracle.address, signature));
  });

  it("only previous large oracle can sign update", async () => {

    // valid update o1 -> o3
    const newOracle = web3.eth.accounts.create();
    const newNodes = createNewOracle();
    const merkleTree = createLargeOracleMerkleTree(newNodes).getHexRoot();
    const msg = messageDigestLargeOracleUpdate(2,
        {size: 4, merkleRoot: merkleTree, oracle: newOracle.address});
    const signature = signWithLargeOracle(msg);

    // update o1 -> o2
    await updateLargeOracle();

    // update o2 -> o3 fails. Although o3 has a higher nonce, it is not signed by o2 (but by o1)
    // and so is not valid.
    await truffleAssert.reverts(
        largeOracleContract.update(4, merkleTree, newOracle.address, signature),
        "Invalid signature");
  });

// create a proof that address is part of the current large oracle
  function createProofForNode(publicKey) {
    let sha = hash.sha256();
    sha.update(publicKey);
    let hashedAddress = Buffer.from(sha.digest("hex"), "hex");
    return largeOracleMerkleRoot.getHexProof(hashedAddress);
  }

  function signWith(msg, signer) {
    const sig = ethUtil.ecsign(msg, signer);
    return Buffer.from(ethUtil.toRpcSig(sig.v, sig.r, sig.s).substring(2), "hex");
  }

  function sendExceptionalUpdate(nonce, newLargeOracle, nodeIndex) {
    const msg = messageDigestLargeOracleUpdate(nonce, newLargeOracle);
    const proof = createProofForNode(largeOracleNodePubKeys[nodeIndex]);
    const sig = signWith(msg, largeOracleNodeKeys[nodeIndex]);
    return largeOracleContract.updateExceptional(
        newLargeOracle.size,
        newLargeOracle.merkleRoot,
        newLargeOracle.oraclePubKey,
        largeOracleNodePubKeys[nodeIndex],
        proof,
        sig);
  }

  it("exceptional update works as expected", async () => {
    const newOracle = web3.eth.accounts.create();
    const newNodes = createNewOracle();
    const merkleTree = createLargeOracleMerkleTree(newNodes).getHexRoot();
    const newOraclePrvKey = Buffer.from(newOracle.privateKey.substring(2), "hex");
    const newLargeOracle = {
      size: 4,
      merkleRoot: merkleTree,
      oraclePubKey: ethUtil.privateToPublic(newOraclePrvKey),
    };

    await sendExceptionalUpdate(1, newLargeOracle, 0);
    await sendExceptionalUpdate(1, newLargeOracle, 1);
    await sendExceptionalUpdate(1, newLargeOracle, 3);

    const nonce = await largeOracleContract.largeNonce();
    assert.equal(nonce, 1);
  });

  it("exceptional update node cannot vote twice", async () => {
    const newOracle = web3.eth.accounts.create();
    const newNodes = createNewOracle();
    const merkleTree = createLargeOracleMerkleTree(newNodes).getHexRoot();
    const newOraclePrvKey = Buffer.from(newOracle.privateKey.substring(2), "hex");
    const newLargeOracle = {
      size: 4,
      merkleRoot: merkleTree,
      oraclePubKey: ethUtil.privateToPublic(newOraclePrvKey),
    };

    await sendExceptionalUpdate(1, newLargeOracle, 0);
    await truffleAssert.reverts(sendExceptionalUpdate(1, newLargeOracle, 0),
        "Signer already signed this nonce");
  });

  it("exceptional update unknown node cannot cannot vote", async () => {
    const newOracle = web3.eth.accounts.create();
    const newOraclePubKey = ethUtil.privateToPublic(
        Buffer.from(newOracle.privateKey.substring(2), "hex"));
    const newNodes = createNewOracle();
    const merkleTree = createLargeOracleMerkleTree(newNodes).getHexRoot();
    const newLargeOracle = {size: 4, merkleRoot: merkleTree, oraclePubKey: newOraclePubKey};

    const msg = messageDigestLargeOracleUpdate(1, newLargeOracle);
    const proof = createProofForNode(largeOracleNodePubKeys[0]);
    const sig = signWith(msg, largeOraclePrvKey);
    await truffleAssert.reverts(largeOracleContract.updateExceptional(
            newLargeOracle.size,
            newLargeOracle.merkleRoot,
            newLargeOracle.oraclePubKey,
            largeOraclePubKey,
            proof,
            sig),
        "Signer not part of oracle");
  });

  it("exceptional update inconsistent signer and provided key", async () => {
    const newOracle = web3.eth.accounts.create();
    const newOraclePubKey = ethUtil.privateToPublic(
        Buffer.from(newOracle.privateKey.substring(2), "hex"));
    const newNodes = createNewOracle();
    const merkleTree = createLargeOracleMerkleTree(newNodes).getHexRoot();
    const newLargeOracle = {size: 4, merkleRoot: merkleTree, oraclePubKey: newOraclePubKey};

    const msg = messageDigestLargeOracleUpdate(1, newLargeOracle);
    const proof = createProofForNode(largeOracleNodePubKeys[0]);
    const sig = signWith(msg, largeOraclePrvKey);
    await truffleAssert.reverts(largeOracleContract.updateExceptional(
            newLargeOracle.size,
            newLargeOracle.merkleRoot,
            newLargeOracle.oraclePubKey,
            largeOracleNodePubKeys[0],
            proof,
            sig),
        "Incorrect key provided");
  });

// This "test" is used to generate some information that is used in PBC to ensure that we end up
// with the same message when signing a new oracle. To run, replace "xit" with "it.only".
  xit("reference for bp-orchestration", () => {
    const newOracle = web3.eth.accounts.create();
    const newNodes = createNewOracle();
    const newNodeKeys = [];
    for (let i = 0; i < 4; i++) {
      const prv = Buffer.from(newNodes[i].privateKey.substring(2), "hex");
      newNodeKeys[i] = ethUtil.privateToPublic(prv).toString("hex");
    }
    const merkleTree = createLargeOracleMerkleTree(newNodes);
    const newOraclePrvKey = Buffer.from(newOracle.privateKey.substring(2), "hex");
    const newLargeOracle = {
      size: 4,
      merkleRoot: merkleTree.getHexRoot(),
      oraclePubKey: ethUtil.privateToPublic(newOraclePrvKey),
    };

    const msg = messageDigestLargeOracleUpdate(1, newLargeOracle);

    console.log("Large oracle contract address:");
    console.log(largeOracleContract.address);
    console.log("----------");
    console.log("Oracle:");
    console.log(newLargeOracle.size);
    console.log(newLargeOracle.merkleRoot);
    console.log(newLargeOracle.oraclePubKey.toString("hex"));
    console.log("----------");
    console.log("MerkleTree:");
    console.log(merkleTree);
    console.log("----------");
    console.log("Node public keys:");
    console.log(newNodeKeys);
    console.log("----------");
    console.log("Message:");
    console.log(msg.toString("hex"));
  });

  function assertNonce(tx, nonce) {
    const {logs} = tx;
    assert.ok(Array.isArray(logs));
    assert.equal(logs.length, 1);

    const log = logs[0];
    assert.equal(log.event, "Deposit");
    assert.equal(log.args.nonce, nonce);
  }
})
;
