const WithdrawAndDepositErc20Pbc = artifacts.require("WithdrawAndDepositErc20Pbc");
const LargeOracle = artifacts.require("LargeOracle");
const ERC1967Proxy = artifacts.require("ERC1967Proxy");
const truffleAssert = require("truffle-assertions");
const hash = require("hash.js");
const BN = require("bn.js");
const ethUtil = require("ethereumjs-util");
const {MerkleTree} = require("merkletreejs");

contract("WithdrawAndDepositErc20Pbc", accounts => {
  let contract;
  let contractImplementation;
  let largeOracleContract;
  let largeOracleImplementation;
  let oracles = [];
  let oraclePrvKeys = [];
  let largeOracle;
  let largeOracleMerkleRoot;
  let largeOraclePrvKey;
  let largeOraclePubKey;
  let largeOracleNodes = [];
  let erc20Name = "Wrapped PBC BYOC ETH";
  let erc20Symbol = "PBC_BYOC_ETH";
  let erc20Decimals = 10;
  let domainSeparator = hash.sha256().update("Partisiablockchain").digest("hex");
  // It is necessary to not use too big numbers, since apparently the sender is reused during the
  // entire test suite, so it may run out of gas before all tests have finished. Figure out if
  // there is some way to increase gas for truffle test sender.
  const minDeposit = web3.utils.toWei(new BN("1000000"), "gwei"); // 0.001 eth
  const maxDeposit = web3.utils.toWei(new BN("100000000"), "gwei"); // 0.1 eth
  const maxWithdraw = web3.utils.toWei(new BN("500000000"), "gwei"); // 0.5 eth
  const pbcAccount = "0x00339896577caf2fc1cdcb1b144683e1312b791c24";

  function initializeLargeOracle() {
    largeOracle = web3.eth.accounts.create();
    largeOraclePrvKey = Buffer.from(largeOracle.privateKey.substring(2), "hex");
    largeOraclePubKey = ethUtil.privateToPublic(largeOraclePrvKey);
    for (let i = 0; i < 4; i++) {
      largeOracleNodes[i] = web3.eth.accounts.create();
    }
    largeOracleMerkleRoot = createLargeOracleMerkleTree(largeOracleNodes);
  }

  beforeEach(async () => {
    oracles[0] = web3.eth.accounts.create();
    oracles[1] = web3.eth.accounts.create();
    oracles[2] = web3.eth.accounts.create();
    oraclePrvKeys[0] = Buffer.from(oracles[0].privateKey.substring(2), "hex");
    oraclePrvKeys[1] = Buffer.from(oracles[1].privateKey.substring(2), "hex");
    oraclePrvKeys[2] = Buffer.from(oracles[2].privateKey.substring(2), "hex");
    initializeLargeOracle();
    largeOracleImplementation = await LargeOracle.new();
    const largeOracleInit = largeOracleImplementation.contract.methods.initialize(4,
        largeOracleMerkleRoot.getHexRoot(),
        largeOracle.address, Buffer.from(domainSeparator, "hex")).encodeABI();
    const largeOracleContractProxy = await ERC1967Proxy.new(largeOracleImplementation.address,
        largeOracleInit);
    largeOracleContract = await LargeOracle.at(largeOracleContractProxy.address);

    contractImplementation = await WithdrawAndDepositErc20Pbc.new();
    const init = contractImplementation.contract.methods.initialize(
        minDeposit,
        maxDeposit,
        maxWithdraw,
        oracles.map(({address}) => address),
        largeOracleContract.address,
        erc20Name,
        erc20Symbol,
        erc20Decimals).encodeABI();
    const contractProxy = await ERC1967Proxy.new(contractImplementation.address, init);
    contract = await WithdrawAndDepositErc20Pbc.at(contractProxy.address);

  });

  it("Symbol, name and decimal is correct", async () => {
    let name = await contract.name();
    let symbol = await contract.symbol();
    let decimal = await contract.decimals();
    assert.equal(symbol, erc20Symbol);
    assert.equal(name, erc20Name);
    assert.equal(decimal, erc20Decimals);
  });

   it("Mint and burn ERC20 tokens on contract", async () => {
    let withdrawalAmount = web3.utils.toWei(new BN("400000000"), "gwei");
    let depositAmount = web3.utils.toWei(new BN("90000000"), "gwei");
    let address = accounts[0];
    let balance = await contract.balanceOf(address);
    assert.equal(balance, 0);

    await assertValidWithdrawal(balance,1, address, withdrawalAmount, 0, 0b111);

    let newBalance = await contract.balanceOf(address);
    assert.equal(newBalance, withdrawalAmount.toString());

    await assertValidDeposit(depositAmount, newBalance, address);

    let totalSupply = await contract.totalSupply();
    assert.equal(totalSupply, withdrawalAmount.sub(depositAmount).toString(10));
  });

  it("A deposit with empty balances does nothing", async () => {
    let address = accounts[0];
    let balance = await contract.balanceOf(address);
    assert.equal(balance, 0);

    // Creates a deposit from sender
    await truffleAssert.reverts(contract.deposit(pbcAccount, maxDeposit, {from: address}),
        "ERC20: burn amount exceeds balance");

    let totalSupply = await contract.totalSupply();
    assert.equal(totalSupply, new BN(0).toString(10));
  });

  async function assertValidDeposit(depositAmount, currentBalance, sender) {
    // Creates a deposit from sender
    const tx = await contract.deposit(pbcAccount, depositAmount, {from: sender});

    // Two logs: one for the burn, and one for the Deposit
    const {logs} = tx;
    assert.ok(Array.isArray(logs));
    assert.equal(logs.length, 2);

    const logTransaction = logs[0];
    assert.equal(logTransaction.event, "Transfer");
    assert.equal(logTransaction.args.to, "0x0000000000000000000000000000000000000000");
    assert.equal(logTransaction.args.from, sender);
    assert.equal(logTransaction.args.value.toString(10), depositAmount.toString(10));

    const log = logs[1];
    assert.equal(log.event, "Deposit");
    assert.equal(log.args.nonce, 0);
    assert.equal(log.args.destination, pbcAccount);
    assert.equal(log.args.amount.toString(10), depositAmount.toString(10));
    let balance = await contract.balanceOf(sender);
    assert.equal(currentBalance.sub(depositAmount).toString(10), balance);
  }

  function hashAddress(sha, address) {
    sha.update(web3.utils.hexToBytes(address));
  }

  function hashNumber(sha, number, size = 64) {
    const n = web3.utils.padLeft(web3.utils.numberToHex(number), size);
    sha.update(web3.utils.hexToBytes(n));
  }

  function messageDigestWithdraw(oracleNonce, nonce, destAddress, withdraw) {
    let sha = hash.sha256();

    hashAddress(sha, contract.address);
    hashNumber(sha, oracleNonce, 16);
    hashNumber(sha, nonce, 16);
    hashAddress(sha, destAddress);
    hashNumber(sha, withdraw);

    return Buffer.from(sha.digest("hex"), "hex");
  }

  function sign(msgDigest, toSkip) {
    const sigs = [];
    if (toSkip === undefined) {
      toSkip = [];
    }
    for (let i = 0; i < oraclePrvKeys.length; i++) {
      if (toSkip.includes(i)) {
        continue;
      }
      const oraclePrvKey = oraclePrvKeys[i];
      const sig = ethUtil.ecsign(msgDigest, oraclePrvKey);
      sigs.push(Buffer.from(ethUtil.toRpcSig(sig.v, sig.r, sig.s).substring(2), "hex"));
    }
    return Buffer.concat(sigs);
  }

  async function assertValidWithdrawal(currentBalance, nonce, receiver, amount, oracleNonce, bitmask) {
    // Creates a withdrawal
    let msgDigest = messageDigestWithdraw(oracleNonce, nonce, receiver, amount);
    let signatures = sign(msgDigest);

    const {logs} = await contract.withdraw(nonce, receiver, amount, 0b111, signatures);
    // Two logs: one for the withdrawal and one for the transfer event of minting.
    assert.ok(Array.isArray(logs));
    assert.equal(logs.length, 2);

    const log = logs[0];
    assert.equal(log.event, "Withdrawal");
    assert.equal(log.args.withdrawalNonce, nonce);
    assert.equal(log.args.destination, receiver);
    assert.equal(log.args.amount.toString(10), amount.toString(10));
    assert.equal(log.args.oracleNonce, oracleNonce);
    assert.equal(log.args.bitmask, bitmask);

    const logTransaction = logs[1];
    assert.equal(logTransaction.event, "Transfer");
    assert.equal(logTransaction.args.from, "0x0000000000000000000000000000000000000000");
    assert.equal(logTransaction.args.to, receiver);
    assert.equal(logTransaction.args.value.toString(10), amount.toString(10));
    let balance = await contract.balanceOf(receiver);
    assert.equal(currentBalance.add(amount).toString(10), balance);

  }

  function merkleTreeSha256(data) {
    return Buffer.from(hash.sha256().update(data).digest("hex"), "hex");
  }

  function createLargeOracleMerkleTree(nodes) {
    const hashed = [];
    for (let i = 0; i < nodes.length; i++) {
      let prv = Buffer.from(nodes[i].privateKey.substring(2), "hex");
      let sha = hash.sha256();
      sha.update(ethUtil.privateToPublic(prv));
      hashed[i] = Buffer.from(sha.digest("hex"), "hex");
    }
    return new MerkleTree(hashed, merkleTreeSha256, {sortPairs: true, sortLeaves: true});
  }
});
