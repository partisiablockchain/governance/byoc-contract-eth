set -v
TIMESTAMP=$(date +%s)
VERSION=$(cat version.txt)
PACKAGE_NAME="byoc-contract-eth"
FILENAME="${PACKAGE_NAME}-${VERSION}.tar.gz"
URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGE_NAME}/${VERSION}/${FILENAME}"

echo "Checking if version already exists: ${URL}"
STATUS_CODE=$(curl -w "CODE: %{http_code}" --header "JOB-TOKEN: $CI_JOB_TOKEN" -I "${URL}" | grep 'CODE: 404')
if [[ "${STATUS_CODE}" != "CODE: 404" ]]; then
  echo "File '${URL}' already exists in repository, please bump the version."
  exit 1
fi

BRANCH=${CI_COMMIT_REF_NAME}
if [[ "$BRANCH" != "main" ]]; then
  # Replace potential forward slashes in branch name with underscores
  VERSION=${VERSION}-${BRANCH//\//_}-${TIMESTAMP}
  FILENAME="${PACKAGE_NAME}-${VERSION}.tar.gz"
  URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGE_NAME}/${VERSION}/${FILENAME}"
fi

echo "Packaging ${FILENAME}"
tar -cjaf "$FILENAME" build/contracts/

echo "Uploading ${FILENAME} to ${URL}"
curl -w "%{http_code}" --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "${FILENAME}" "${URL}" >status.log

cat status.log
grep -E "(200|201|204)" status.log
